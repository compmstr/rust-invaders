use components::Component;

pub struct Health{
    id: uint,
    hp: int
}
impl Component for Health{
    fn get_id(&self) -> uint { self.id }
}
