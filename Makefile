RUSTC=rustc
RUSTFLAGS=-A unused_variable -A dead_code
LDFLAGS=-L lib

.PHONY : all clean doc

all: clean compile

debug: RUSTFLAGS += -g -Z time-passes
debug: compile

fast: RUSTFLAGS += -O
fast: compile

compile:
	mkdir -p bin
	$(RUSTC) $(RUSTFLAGS) -o bin/rust-invaders $(LDFLAGS) src/main.rs

deps:
	git submodule update --init
	rm -f lib/libsdl2*
	cd lib/rust-sdl2; make clean && make
	cp lib/rust-sdl2/build/lib/libsdl2* lib/

doc:
	rustdoc $(LDFLAGS) src/main.rs

clean:
	rm -f bin/**

run:
	bin/rust-invaders
