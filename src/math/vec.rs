use std::num::Float;
use std::f64::consts::PI;
#[deriving(Clone, Decodable)]
pub struct Vec2{pub x: f64, pub y: f64}

static DEG_TO_RAD: f64 = PI / 180.0;
#[allow(dead_code)]
impl Vec2{
    pub fn new(x: f64, y: f64) -> Vec2 {
        Vec2{x: x, y: y}
    }
    pub fn square(size: f64) -> Vec2 {
        Vec2::new(size, size)
    }
    pub fn dot(&self, other: Vec2) -> f64{
        self.x * other.x + self.y * other.y
    }
    pub fn dist(&self, other: Vec2) -> f64{
        self.dot(other).sqrt()
    }
    pub fn mag(&self) -> f64 {
        self.dot(*self).sqrt()
    }
    pub fn mag_squared(&self) -> f64 {
        self.dot(*self)
    }
    pub fn norm(&self) -> Vec2 {
        let mag_inv = 1.0 / self.mag();
        self * mag_inv
    }
    pub fn rotate(&self, angle_deg: f64) -> Vec2{
        let angle = angle_deg * DEG_TO_RAD;
        let s = angle.sin();
        let c = angle.cos();
        Vec2::new((self.x * c) - (self.y * s),
                  (self.x * s) + (self.y * c))
    }
}
impl Add<Vec2, Vec2> for Vec2{
    fn add(&self, rhs: &Vec2) -> Vec2{
        Vec2::new(self.x + rhs.x, self.y + rhs.y)
    }
}
impl Sub<Vec2, Vec2> for Vec2{
    fn sub(&self, rhs: &Vec2) -> Vec2{
        Vec2::new(self.x - rhs.x, self.y - rhs.y)
    }
}
impl Mul<f64, Vec2> for Vec2{
    fn mul(&self, rhs: &f64) -> Vec2{
        Vec2::new(self.x * *rhs, self.y * *rhs)
    }
}
