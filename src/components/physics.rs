use math::vec::Vec2;

use components::Component;

pub struct Position{
    pub id: uint,
    pub pos: Vec2,
    pub angle: f64
}
impl Component for Position{
    fn get_id(&self) -> uint { self.id }
}
pub struct Velocity{
    pub id: uint,
    pub vel: Vec2,
    pub rot_speed: f64,
}
impl Component for Velocity{
    fn get_id(&self) -> uint { self.id }
}

pub struct Acceleration{
    pub id: uint,
    pub accel: Vec2,
    pub max_speed: Option<f64>,
    pub rot_accel: f64,
    pub max_rot_speed: Option<f64>,
}
impl Component for Acceleration{
    fn get_id(&self) -> uint { self.id }
}

pub struct Size{
    pub id: uint,
    pub size: Vec2
}
impl Component for Size{
    fn get_id(&self) -> uint { self.id }
}
