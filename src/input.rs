extern crate sdl2;
use collections::hashmap::HashMap;
use broadcast::Broadcaster;

#[deriving(Hash, Eq, Clone, TotalEq)]
pub enum KeyAction{
    MoveLeft,
    MoveRight,
    MoveUp,
    MoveDown,
    Fire,
    Exit
}
#[deriving(Clone)]
pub enum ActionMessage{
    Press(KeyAction),
    Release(KeyAction),
    KeyType(char)
}
pub struct Input{
    key_map: HashMap<sdl2::keycode::KeyCode, KeyAction>,
    pressed_actions: HashMap<KeyAction, bool>,
    released_actions: HashMap<KeyAction, bool>,
    held_actions: HashMap<KeyAction, bool>,
    update_broadcaster: Broadcaster<ActionMessage>
}
impl Input {
    pub fn new() -> Input {
        let mut ret = Input{
            key_map: HashMap::new(),
            pressed_actions: HashMap::new(),
            released_actions: HashMap::new(),
            held_actions: HashMap::new(),
            update_broadcaster: Broadcaster::new()
        };
        
        ret.insert_key_action(MoveRight, sdl2::keycode::DKey);
        ret.insert_key_action(MoveLeft, sdl2::keycode::AKey);
        ret.insert_key_action(MoveUp, sdl2::keycode::WKey);
        ret.insert_key_action(MoveDown, sdl2::keycode::SKey);
        ret.insert_key_action(Fire, sdl2::keycode::SpaceKey);
        ret.insert_key_action(Exit, sdl2::keycode::EscapeKey);
        
        ret
    }
    
    pub fn listen(&mut self) -> ~Receiver<ActionMessage> {
        self.update_broadcaster.listen()
    }
    
    pub fn on_key_down(&mut self, keycode: sdl2::keycode::KeyCode){
        //If we have a printable character...
        if sdl2::keycode::SpaceKey as int <= keycode as int &&
           keycode as int <= sdl2::keycode::ZKey as int {
            //TODO: doesn't handle uppercase properly
            self.update_broadcaster.send(&KeyType(keycode as u8 as char));
        }
        match self.key_map.find(&keycode){
            Some(action) => {
                if !self.is_action_held(*action) {
                    self.update_broadcaster.send(&Press(*action));
                }
                self.pressed_actions.insert(*action, true);
                self.held_actions.insert(*action, true);
            },
            None => {}
        }
    }
    pub fn on_key_up(&mut self, keycode: sdl2::keycode::KeyCode){
        match self.key_map.find(&keycode){
            Some(action) => {
                self.update_broadcaster.send(&Release(*action));
                self.released_actions.insert(*action, true);
                self.held_actions.insert(*action, false);
            },
            None => {}
        }
    }

    pub fn insert_key_action(&mut self, action: KeyAction, keycode: sdl2::keycode::KeyCode){
        self.key_map.insert(keycode, action);
    }
    
    pub fn is_action_pressed(&self, action: KeyAction) -> bool{
        match self.pressed_actions.find(&action){
            Some(cur) => *cur,
            None => false
        }
    }
    pub fn is_action_held(&self, action: KeyAction) -> bool{
        match self.held_actions.find(&action){
            Some(cur) => *cur,
            None => false
        }
    }
    pub fn is_action_released(&self, action: KeyAction) -> bool{
        match self.released_actions.find(&action){
            Some(cur) => *cur,
            None => false
        }
    }
    pub fn new_cycle(&mut self){
        self.pressed_actions.clear();
        self.released_actions.clear();
    }
}
