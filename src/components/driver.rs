use collections::hashmap::HashMap;
use components::Component;
use std::default::Default;

#[deriving(Clone,Hash,TotalEq,Eq)]
pub enum DriverAction{
    Forward,
    Back,
    Left,
    Right,
    TurnLeft,
    TurnRight,
    Fire
}
#[deriving(Clone)]
pub enum DriverActionEvent{
    Start(DriverAction),
    Stop(DriverAction)
}

pub enum InputSource{
    Human,
    AI(uint),
}

pub struct DriverParams{
    pub accel: f64,
    pub rot_accel: f64,
    pub fire_delay: uint,
}
impl DriverParams{
    pub fn new(accel: f64, rot_accel: f64, fire_delay: uint) -> DriverParams{
        DriverParams{accel: accel, rot_accel: rot_accel, fire_delay: fire_delay}
    }
}
impl Default for DriverParams{
    fn default() -> DriverParams{
        DriverParams::new(1000f64, 180f64, 100u)
    }
}
pub struct Driver{
    id: uint,
    pub actions: HashMap<DriverAction, bool>,
    pub inputSource: InputSource,
    pub params: DriverParams,
}
impl Driver{
    pub fn new(id: uint, inputSource: InputSource, params: Option<DriverParams>) -> Driver{
        Driver{id: id, actions: HashMap::new(), inputSource: inputSource, params: params.unwrap_or_default()}
    }
    pub fn update_action(&mut self, dae: &DriverActionEvent){
        match *dae {
            Start(a) => {self.actions.find_or_insert(a, true);},
            Stop(a) => {self.actions.find_or_insert(a, false);},
        };
    }
    pub fn action_status(&self, action: &DriverAction) -> bool {
        match self.actions.find(action) {
            Some(state) => *state,
            None => false,
        }
    }
}
impl Component for Driver{
    fn get_id(&self) -> uint { self.id }
}
