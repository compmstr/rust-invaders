extern crate sync;
extern crate collections;

use sync::{Arc, RWLock};
use collections::hashmap::{HashSet};
use entities::Entities;
use systems::System;

pub struct CollisionSystem{
    ents: Arc<RWLock<Entities>>
}
impl CollisionSystem{
    pub fn new(ents: Arc<RWLock<Entities>>) -> CollisionSystem {
        CollisionSystem{ents: ents}
    }
}
impl System for CollisionSystem{
    fn get_ids(&self) -> ~HashSet<uint> {
        let pos_ids = &self.ents.read().pos.ids.clone();
        let size_ids = &self.ents.read().size.ids.clone();
        ~pos_ids.intersection(size_ids).map(|i| *i).collect()
    }
    fn update(&mut self, d_time: f64){
        let ids = self.get_ids();
    }
}
