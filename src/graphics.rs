extern crate sdl2;
use collections::hashmap::HashMap;
use math::vec::Vec2;
use sdl2::surface;
use sdl2::pixels;
use sdl2::render;
use sdl2::rect;
use sync::Arc;

pub struct Graphics{
    win_size: Vec2,
    rend: ~render::Renderer,
    sprite_cache: HashMap<~str, Arc<~render::Texture>>
}

impl Graphics{
    pub fn new(title: &str, width: int, height: int) -> Graphics{
        let window = match sdl2::video::Window::new(title, sdl2::video::PosCentered,
                                                    sdl2::video::PosCentered, width, height,
                                                    [sdl2::video::OpenGL]){
            Ok(window) => window,
            Err(err) => fail!(format!("failed to create window: {}", err))
        };
        let rend = match render::Renderer::from_window(window, render::DriverAuto,
                                                             [render::Accelerated]){
            Ok(rend) => rend,
            Err(err) => fail!(format!("failed to create rendered: {}", err))
        };
        Graphics{
            win_size: Vec2::new(width as f64, height as f64),
            rend: rend,
            sprite_cache: HashMap::new()
        }
    }
    
    pub fn clear(&self){
        self.rend.set_draw_color(pixels::RGB(0, 0, 0));
        self.rend.clear();
    }
    
    pub fn present(&self){
        self.rend.present();
    }

    pub fn draw_rect(&self, pos: Vec2, size: Vec2, color: pixels::Color){
        let rec = sdl2::rect::Rect(pos.x as i32, pos.y as i32, size.x as i32, size.y as i32);
        self.rend.set_draw_color(color);
        self.rend.fill_rect(&rec);
    }
    
    pub fn load_sprite(&mut self, path: ~str,
                      transparent_color: Option<(u8, u8, u8)>) -> Arc<~render::Texture>{
        let rend = &self.rend;
        let handle = self.sprite_cache.find_or_insert_with(path, |key| {
            //Load sprite
            let sprite_path = Path::new((*key).clone());
            let sprite_surface = match surface::Surface::from_bmp(&sprite_path) {
                Ok(surface) => surface,
                Err(msg) => fail!("sprite could not be loaded: {}", msg),
            };
            //Set transparent color, if needed
            match transparent_color {
                Some((r, g, b)) => 
                    match sprite_surface.set_color_key(true, pixels::RGB(r, g, b)){
                        Ok(_) => {},
                        Err(msg) => fail!("Failed to set key color: {}", msg),
                    },
                None => {}
            }
            //Wrap surface as a texture
            match rend.create_texture_from_surface(sprite_surface) {
                Ok(tex) => Arc::new(tex),
                Err(msg) => fail!("Couldn't render surface to texture: {}", msg)
            }
        });
        handle.clone()
    }
    
    pub fn blit_surface(&self, src: &render::Texture, src_rect: Option<rect::Rect>,
                        dest_rect: rect::Rect){
        self.rend.copy(src, src_rect, Some(dest_rect));
    }
    
    pub fn blit_surface_ex(&self, src: &render::Texture, src_rect: Option<rect::Rect>,
                            dest_rect: rect::Rect, rot_center: Option<rect::Point>, angle: f64, flip: render::RendererFlip){
        self.rend.copy_ex(src, src_rect, Some(dest_rect), angle as f64, rot_center, flip);
    }
}
