use std::vec::Vec;
use std::iter::count;

pub struct Broadcaster<T>{
    listeners: ~Vec<Sender<T>>
}
impl<T: Send + Clone> Broadcaster<T>{
    pub fn new() -> Broadcaster<T>{
        Broadcaster{listeners: ~Vec::new()}
    }
    
    pub fn listen(&mut self) -> ~Receiver<T>{
        let (tx, rx) = channel();
        self.listeners.push(tx);
        ~rx
    }
    
    pub fn send(&mut self, msg: &T){
        //TODO: should be able to use filter to clean this up instead
        //  of the whole to_remove vector thing
        //This gets close, I think
        //*self.listeners = self.listeners.iter().filter(|tx| tx.try_send(msg.clone())).collect();
        let mut to_remove: Vec<uint> = Vec::new();
        for (idx, tx) in count(0, 1).zip(self.listeners.iter()){
            match tx.send_opt(msg.clone()) {
                Ok(_) => {},
                Err(_) => to_remove.push(idx as uint)
            }
        }
        to_remove.reverse();
        for idx in to_remove.iter(){
            self.listeners.remove(*idx);
        }
    }
}
