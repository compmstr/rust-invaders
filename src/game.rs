extern crate sync;

use math::vec::{Vec2};
use input::{Input};
use graphics::Graphics;
use std::result::{Ok, Err};
use sync::{Arc, RWLock};
use entities::Entities;
use systems::System;
use systems::physics::PhysicsSystem;
use systems::collision::CollisionSystem;
use systems::graphics::GraphicsSystem;
use systems::driver::DriverSystem;

pub enum GameEvent{
    SpawnBullet(Vec2 , Vec2),
    SpawnExplosion(Vec2),
    KillEntity(uint)
}

pub struct GameSystems{
    pub physics: PhysicsSystem,
    pub collision: CollisionSystem,
    pub graphics: GraphicsSystem,
    pub driver: DriverSystem,
}

pub struct Game{
    pub entities: Arc<RWLock<Entities>>,
    pub systems: ~GameSystems,
    action_queue_tx: ~Sender<GameEvent>,
    action_queue_rx: ~Receiver<GameEvent>
}

impl Game{
    pub fn new(input: &mut Input, graphics: Arc<RWLock<Graphics>>) -> Game{
        let action_queue = channel();
        let (tx, rx) = action_queue;
        let entities = Arc::new(RWLock::new(Entities::new()));
        Game{ 
            entities: entities.clone(),
            systems: ~GameSystems{
                physics: PhysicsSystem::new(entities.clone()),
                collision: CollisionSystem::new(entities.clone()),
                graphics: GraphicsSystem::new(entities.clone(), graphics),
                driver: DriverSystem::new(entities.clone(), input.listen()),
            },
            action_queue_tx: ~tx,
            action_queue_rx: ~rx
        }
    }
    pub fn handle_events(&mut self){
       'events : loop {
           match self.action_queue_rx.try_recv(){
               Ok(msg) =>{
                   match msg {
                       SpawnBullet(pos, vel) => {
                           println!("Spawning bullet @ {:?}", pos)
                       },
                       SpawnExplosion(pos) => {
                           println!("Boom! @ {:?}", pos);
                       },
                       KillEntity(id) => {
                           self.entities.clone().write().remove(id);
                       },
                   }
               },
               _ => break 'events
           }
       } 
    }
    pub fn update(&mut self, d_time: f64){
        self.handle_events();
        
        self.systems.physics.update(d_time);
        self.systems.collision.update(d_time);
        self.systems.graphics.update(d_time);
        self.systems.driver.update(d_time);
    }
    
    pub fn draw(&self){
        self.systems.graphics.draw();
    }
}
