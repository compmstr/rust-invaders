extern crate sdl2;
extern crate sync;
extern crate collections;

use collections::hashmap::HashMap;
use std::io::fs::File;
use serialize::{json, Decodable};
use math::vec::{Vec2};
use graphics::Graphics;
use sdl2::{rect, render};
use std::vec::Vec;
use sync::Arc;
use components::Component;

pub struct SpriteAnimation{
    repeat: bool,
    frame_delay: uint,
    frame_size: Option<Vec2>,
    frames: Vec<(Vec2, render::RendererFlip)>
}
impl SpriteAnimation{
    pub fn from_json_obj(obj: &json::Json) -> SpriteAnimation{
        let frame_delay = match obj.find(&~"frame_delay"){
            Some(delay) => {
                match delay.as_number() {
                    Some(delay) => delay as uint,
                    None => fail!("frame_delay isn't a number")
                }
            },
            None => fail!("no frame_delay for animation")
        };
        let frame_size = match obj.find(&~"frame_size"){
            Some(size) =>{
                Some(vec2_from_json(size))
            },
            None => None
        };
        let repeat = match obj.find(&~"repeat"){
            Some(repeat) =>
                match repeat.as_boolean(){
                    Some(repeat) => repeat,
                    None => fail!("animation repeat isn't a boolean")
                },
            None => true
        };
        let mut frames: Vec<(Vec2, render::RendererFlip)> = Vec::new();
        match obj.find(&~"frames"){
            Some(frame_list) => {
                match frame_list.as_list(){
                    Some(frame_list) =>
                        for frame in frame_list.iter(){
                            let frame_loc = vec2_from_json(frame);
                            let frame_flip = match frame.find(&~"flip"){
                                Some(flip) => match flip.as_string(){
                                    Some(flip_str) => {
                                        match flip_str {
                                            "x" => render::FlipHorizontal,
                                            "y" => render::FlipVertical,
                                            "both" => render::FlipBoth,
                                            _ => fail!("frame flip isn't x/y/both")
                                        }
                                    },
                                    None => fail!("frame flip isn't a string"),
                                },
                                None => render::FlipNone
                            };
                            frames.push((frame_loc, frame_flip));
                        },
                    None => fail!("frames isn't a list in sprite")
                }
            },
            None => frames.push((Vec2::new(0.0, 0.0), render::FlipNone))
        }
        SpriteAnimation{
            repeat: repeat,
            frame_delay: frame_delay,
            frame_size: frame_size,
            frames: frames
        }
    }
}

//This doesn't work at the moment, because rust treats Option
//  decoding as required 'Null' to be in the text
//TODO: look into implementing missing key -> None conversion
/*
#[deriving(Decodable)]
pub struct FrameJson{
    x: uint,
    y: uint,
    flip: Option<~str>
}
#[deriving(Decodable)]
pub struct AnimationJson{
    frame_delay: uint,
    frames: Vec<FrameJson>,
    frame_size: Option<Vec2>
}

#[deriving(Decodable)]
pub struct SpriteJson{
    image_file: ~str,
    frame_size: Vec2,
    base_anim: ~str,
    animations: HashMap<~str, AnimationJson>
}

impl SpriteJson{
    pub fn from_file(filename: ~str) -> SpriteJson{
        let mut json_file = match File::open(&Path::new(filename.clone())) {
            Err(err) => fail!("Unable to open file: {}", err),
            Ok(file) => file,
        };
        let json_data = match json::from_reader(&mut json_file as &mut Reader) {
            Err(err) => fail!("Json parse error: {}", err),
            Ok(data) => data,
        };
        let mut decoder = json::Decoder::new(json_data);
        let decoded_obj: SpriteJson = Decodable::decode(&mut decoder);
        decoded_obj
    }
}
*/

pub struct Sprite{
    id: uint,
    image: sync::Arc<~sdl2::render::Texture>,
    frame_size: Vec2,
    animation_names: collections::hashmap::HashMap<~str, uint>,
    animations: Vec<SpriteAnimation>,
    animation: uint,
    base_anim: uint,
    frame: uint,
    since_last_frame: uint,
}
impl Component for Sprite{
    fn get_id(&self) -> uint { self.id }
}

fn vec2_from_json(data: &json::Json) -> Vec2{
    let mut decoder = json::Decoder::new(data.clone());
    let decoded_obj: Vec2 = Decodable::decode(&mut decoder).ok().unwrap();
    decoded_obj
}

impl Sprite{
    pub fn set_anim_by_name(&mut self, s: &str){
        self.animation = match self.animation_names.find(&s.into_owned()){
            Some(idx) => *idx,
            None => fail!("Invalid animation specified: {}", s)
        }
    }
    pub fn cur_anim<'a>(&'a self) -> &'a SpriteAnimation{
        self.animations.get(self.animation)
    }
    pub fn cur_size(&self) -> Vec2{
        let anim = self.cur_anim();
        anim.frame_size.unwrap_or(self.frame_size)
    }

    pub fn from_file(graphics: &mut Graphics, id: uint, filename: ~str) -> Sprite{
        let mut json_file = match File::open(&Path::new(filename.clone())) {
            Err(err) => fail!("Unable to open file: {}", err),
            Ok(file) => file,
        };
        let json_data = match json::from_reader(&mut json_file as &mut Reader) {
            Err(err) => fail!("Json parse error: {}", err),
            Ok(data) => data,
        };
        let image_filename = json_data.find(&~"image_file")
            .expect(format!("No image file specified in sprite: {}", filename))
            .as_string().expect(format!("image_file isn't a string in sprite: {}", filename));
        let texture = graphics.load_sprite((image_filename.clone()).to_owned(), Some((255, 0, 255)));
        let base_anim = match json_data.find(&~"base_anim"){
            None => fail!("No base animation set for sprite: {}", filename),
            Some(s) => match s.as_string() {
                None => fail!("base_anim isn't a string in sprite: {}", filename),
                Some(base_anim) => base_anim
            }
        };
        let frame_size = match json_data.find(&~"frame_size"){
            None => fail!("No frame_size set for sprite: {}", filename),
            Some(frame_size) => {
                vec2_from_json(frame_size)
            },
        };
        let mut anims: Vec<SpriteAnimation> = Vec::new();
        let mut anim_names = collections::hashmap::HashMap::new();
        match json_data.find(&~"animations"){
            None => fail!("No Animations in sprite file"),
            Some(json_anims) =>{
                match json_anims.as_object() {
                    None => fail!("Animations in sprite file isn't an object"),
                    Some(json_anims) => {
                        for (name, anim) in json_anims.iter(){
                            let anim = SpriteAnimation::from_json_obj(anim);
                            anim_names.insert(name.clone().into_owned(), anims.len());
                            anims.push(anim);
                        }
                    },
                }
            },
        };
        let base_anim_name = json_data.find(&~"base_anim")
            .expect(format!("No base animation set for sprite: {}", filename))
            .as_string().expect(format!("base_anim isn't a string in sprite: {}", filename))
            .into_owned();
        let base_anim = match anim_names.find(&base_anim_name) {
            Some(idx) => *idx,
            None => fail!("Base animation isn't in the list of animations"),
        };
        Sprite{
            id: id,
            image: texture,
            frame_size: frame_size,
            animation_names: anim_names,
            animations: anims,
            animation: base_anim,
            base_anim: base_anim,
            frame: 0,
            since_last_frame: 0,
        }
    }

    pub fn draw(&self, graphics: &Graphics, pos: Vec2, angle: f64){
        let size = self.cur_size();
        let dest_rect = rect::Rect::new(pos.x as i32, pos.y as i32, 
                                        size.x as i32, size.y as i32);
        let (anim_frame, frame_flip) = *(self.cur_anim().frames.get(self.frame));
        let src_rect = rect::Rect::new(anim_frame.x as i32, anim_frame.y as i32,
                                       size.x as i32, size.y as i32);
        graphics.blit_surface_ex(*self.image, Some(src_rect), dest_rect, None, angle, frame_flip);
    }
    pub fn size(&self) -> Vec2{
        self.cur_size()
    }
    pub fn update(&mut self, d_time: f64) -> bool{
        let mut done = false;
        self.since_last_frame += (d_time * 1000.0) as uint;
        let (frame_delay, num_frames, repeat) = {
            //This allows me to pull data out of self, and then use it 
            //  mutably later on
            let cur_anim = self.cur_anim();
            (cur_anim.frame_delay, cur_anim.frames.len(), cur_anim.repeat)
        };
        if self.since_last_frame >= frame_delay {
            self.since_last_frame = 0;
            let new_frame = self.frame + 1;
            if new_frame >= num_frames {
                self.frame = 0;
                if !repeat {
                    self.animation = self.base_anim.clone();
                    done = true;
                }            }else{
                self.frame = new_frame;
            }
        }
        done
    }
}
