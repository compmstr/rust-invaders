extern crate sync;
extern crate collections;

use sync::{Arc, RWLock};
use collections::hashmap::{HashSet};
use entities::Entities;
use systems::System;
use graphics::Graphics;
use math::vec::Vec2;

pub struct GraphicsSystem{
    ents: Arc<RWLock<Entities>>,
    graphics: Arc<RWLock<Graphics>>,
}
impl GraphicsSystem{
    pub fn new(ents: Arc<RWLock<Entities>>, graphics: Arc<RWLock<Graphics>>) -> GraphicsSystem {
        GraphicsSystem{ents: ents, graphics: graphics}
    }
    pub fn draw(&self){
        let ids = self.get_ids();
        let r = self.ents.read();
        'draw_loop: for cur_id in ids.iter(){
            let id = *cur_id;
            let pos = match r.pos.find(id){
                Some(p) => p.read(),
                None => continue 'draw_loop
            };
            let sprite = match r.sprite.find(id){
                Some(s) => s.read(),
                None => continue 'draw_loop
            };
            sprite.draw(&*self.graphics.read(), self.world_to_screen(pos.pos), pos.angle);
        }
    }
    fn world_to_screen(&self, loc: Vec2) -> Vec2 {
        Vec2::new(loc.x, -loc.y + 600.0)
    }
}
impl System for GraphicsSystem{
    fn get_ids(&self) -> ~HashSet<uint> {
        //Gather position and sprite components
        let pos_ids = &self.ents.read().pos.ids.clone();
        let sprite_ids = &self.ents.read().sprite.ids.clone();
        ~pos_ids.intersection(sprite_ids).map(|i| *i).collect()
    }
    fn update(&mut self, d_time: f64){
        let ids = self.get_ids();
        let r = self.ents.read();
        'update_loop: for cur_id in ids.iter(){
            let id = *cur_id;
            let mut sprite = match r.sprite.find(id){
                Some(s) => s.write(),
                None => continue 'update_loop
            };
            sprite.update(d_time);
        }
    }
}
