extern crate collections;
extern crate sync;

use collections::hashmap::{HashMap, HashSet};
use sync::{RWLock};

pub mod physics;
pub mod game;
pub mod sprite;
pub mod driver;

pub trait Component: Send + Share{
    fn get_id(&self) -> uint;
}

pub struct ComponentContainer<T>{
    pub ids: HashSet<uint>,
    id_to_component: HashMap<uint, uint>,
    components: Vec<RWLock<T>>,
}

impl<T: Component + Share> ComponentContainer<T>{
    pub fn new() -> ComponentContainer<T> {
        ComponentContainer{
            ids: HashSet::new(),
            id_to_component: HashMap::new(),
            components: Vec::new()
        }
    }
    pub fn find<'a>(&'a self, id: uint) -> Option<&'a RWLock<T>> {
        self.id_to_component.find(&id)
            .map(|idx| self.components.get(*idx))
    }
    pub fn add(&mut self, val: T){
        self.id_to_component.insert(val.get_id(), self.components.len());
        self.ids.insert(val.get_id());
        self.components.push(RWLock::new(val));
    }
    pub fn remove(&mut self, id: uint){
        let idx = {
            match self.id_to_component.find(&id) {
                Some(idx) => *idx,
                None => return
            }
        };
        self.components.remove(idx);
        self.ids.remove(&id);
        for (id, cur_idx) in self.id_to_component.mut_iter() {
            if *cur_idx > idx {
                *cur_idx = *cur_idx - 1;
            }
        }
        self.id_to_component.remove(&id);
    }
}
