#![crate_id="rust-invaders#0.0.1"]

extern crate sdl2;
extern crate collections;
extern crate sync;
extern crate serialize;

use input::Input;
use graphics::Graphics;

use sync::{Arc, RWLock};
mod graphics;
mod math;
mod input;
mod player;
mod broadcast;
mod game;
mod components;
mod systems;
mod entities;


pub fn main(){
    sdl2::init([sdl2::InitVideo]);
    
    let graphics = Arc::new(RWLock::new(Graphics::new("Rust Invaders", 800, 600)));
    let mut input = ~Input::new();
    
    let mut game = ~game::Game::new(input, graphics.clone());
    
    fn create_player(pos: math::vec::Vec2, entities: Arc<RWLock<entities::Entities>>, graphics: Arc<RWLock<Graphics>>) -> uint {
        entities.write().create(|id, ents| {
            let sprite = components::sprite::Sprite::from_file(&mut *graphics.write(), id, ~"assets/player_ship.sprite");
            ents.pos.add(components::physics::Position{id: id, pos: pos, angle: 0.0});
            ents.vel.add(components::physics::Velocity{id: id, vel: math::vec::Vec2::new(0.0, 0.0), rot_speed: 0.0});
            ents.size.add(components::physics::Size{id: id, size: sprite.cur_size()});
            ents.accel.add(components::physics::Acceleration{id: id, accel: math::vec::Vec2::new(0.0, 0.0), max_speed: Some(500.0), rot_accel: 0.0, max_rot_speed: None});
            ents.sprite.add(sprite);
            ents.driver.add(components::driver::Driver::new(id, components::driver::Human, None));
        })
    }

    let player_id = create_player(math::vec::Vec2::new(400f64, 50f64), game.entities.clone(), graphics.clone());
    println!("player id: {} \n-- player pos: {:?} \n-- player vel: {:?}", player_id, *game.entities.read().pos.find(player_id).unwrap().read(), *game.entities.read().vel.find(player_id).unwrap().read());
    
    fn create_enemy(pos: math::vec::Vec2, entities: Arc<RWLock<entities::Entities>>, graphics: Arc<RWLock<Graphics>>) -> uint {
        entities.write().create(|id, ents| {
            let sprite = components::sprite::Sprite::from_file(&mut *graphics.write(), id, ~"assets/enemy_1.sprite");
            ents.pos.add(components::physics::Position{id: id, pos: pos, angle: 180.0});
            ents.vel.add(components::physics::Velocity{id: id, vel: math::vec::Vec2::new(0.0, 0.0), rot_speed: 0.0});
            ents.size.add(components::physics::Size{id: id, size: sprite.cur_size()});
            ents.accel.add(components::physics::Acceleration{id: id, accel: math::vec::Vec2::new(0.0, 0.0), max_speed: Some(1000.0), rot_accel: 0.0, max_rot_speed: None});
            ents.sprite.add(sprite);
        })
    }
    
    for x in range(0, 14){
        for y in range(0, 8) {
            let id = create_enemy(
                math::vec::Vec2::new((x as f64 * 50f64) + 50f64, (y as f64 * 50f64) + 250f64),
                game.entities.clone(), graphics.clone());
            {
                let r = game.entities.read();
                let enemy_vel = &mut *r.vel.find(id).unwrap().write();
                enemy_vel.vel = enemy_vel.vel + math::vec::Vec2::new(0f64, -10f64);
            }
        }
    }

    let mut last_frame = sdl2::get_ticks();
    let mut d_time: f64;
    //FPS calc
    let mut d_time_sum = 0.0;
    let mut cur_d_time = 0;
    //End fPS calc
    'main : loop {
        input.new_cycle();
        'event : loop{
            match sdl2::event::poll_event(){
                sdl2::event::QuitEvent(_) => break 'main,
                sdl2::event::NoEvent => break 'event,
                sdl2::event::KeyDownEvent(_, _, k, _, _) =>
                    input.on_key_down(k),
                sdl2::event::KeyUpEvent(_, _, k, _, _) =>
                    input.on_key_up(k),
                _ => {}
            };
        }
        if input.is_action_pressed(input::Exit) {
            break 'main
        }
        let new_time = sdl2::get_ticks();
        d_time = ((new_time - last_frame) as f64) / 1000.0;
        last_frame = new_time;
        //FPS calc
        d_time_sum += d_time;
        cur_d_time += 1;
        if cur_d_time == 1024 {
            let fps = 1024.0 / d_time_sum;
            println!("FPS: {}", fps);
            d_time_sum = 0.0;
            cur_d_time = 0;
        }
        //End FPS calc
        
        game.update(d_time);
        
        {
            let g = graphics.read();
            g.clear();
            game.draw();
            g.present();
        }
    }

    sdl2::quit();
}
