extern crate sync;
extern crate collections;

use sync::{Arc, RWLock};
use collections::hashmap::{HashSet};
use entities::Entities;
use systems::System;
use components::physics::{Velocity, Position};
use std::num::abs;

pub struct PhysicsSystem{
    ents: Arc<RWLock<Entities>>
}
impl PhysicsSystem{
    pub fn new(ents: Arc<RWLock<Entities>>) -> PhysicsSystem {
        PhysicsSystem{ents: ents}
    }
}

impl System for PhysicsSystem{
    fn get_ids(&self) -> ~HashSet<uint> {
        let pos_ids: HashSet<uint> = self.ents.read().pos.ids.clone();
        let vel_ids: HashSet<uint> = self.ents.read().vel.ids.clone();
        ~pos_ids.intersection(&vel_ids).map(|i| *i).collect()
    }
    fn update<'a>(&'a mut self, d_time: f64){
        //Gather position and velocity components
        let ids = self.get_ids();
        let ents = &self.ents.read();
        for cur_id in ids.iter() {
            let id = *cur_id;

            let vel_opt = ents.vel.find(id);
            if vel_opt.is_none() { continue; }
            let vel: &mut Velocity = &mut *vel_opt.unwrap().write();

            let pos_opt = ents.pos.find(id);
            if pos_opt.is_none() { continue; }
            let pos: &mut Position = &mut *pos_opt.unwrap().write();
            
            match ents.accel.find(id){
                Some(accel) => {
                    let acc = accel.read();
                    let accelerating = abs(acc.accel.mag_squared()) > 0.1;
                    let rotating = abs(acc.rot_accel) > 0.1;
                    if rotating {
                        vel.rot_speed = vel.rot_speed + (acc.rot_accel * d_time);
                        match acc.max_rot_speed {
                            Some(m) => {
                                let rot_ratio = (m * m) / (vel.rot_speed * vel.rot_speed);
                                if rot_ratio < 1.0 {
                                    vel.rot_speed = m;
                                }
                            },
                            None => {}
                        };
                    } else {
                        //Rotation damping
                        vel.rot_speed *= d_time * (8f64 * -vel.rot_speed);
                    }
                    if accelerating { 
                        vel.vel = vel.vel + (acc.accel.rotate(pos.angle) * d_time);
                        match acc.max_speed {
                            Some(m) => {
                                let speed_ratio = (m * m) / (vel.vel.dot(vel.vel));
                                if speed_ratio < 1.0{
                                    vel.vel = vel.vel * speed_ratio.sqrt();
                                }
                            },
                            None => {}
                        };
                    } else {
                        //Velocity damping
                        let opp = vel.vel * -8f64;
                        vel.vel = vel.vel + (opp * d_time);
                    }
                    
                },
                None => {}
            }
            
            pos.pos = pos.pos + (vel.vel * d_time);
            pos.angle = pos.angle + (vel.rot_speed * d_time);
        }
    }
}
