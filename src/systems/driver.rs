extern crate sync;

use input::{ActionMessage, KeyAction};
use input;
use collections::hashmap::HashSet;
use components::driver::{DriverActionEvent, DriverAction};
use components::driver;
use systems::System;
use std::vec::Vec;
use entities::Entities;
use sync::{Arc, RWLock};

//TODO
pub struct AIState;

pub struct DriverSystem{
    ents: Arc<RWLock<Entities>>,
    inputQueue: ~Receiver<ActionMessage>,
    ais: Vec<AIState>
}

impl DriverSystem{
    pub fn new(ents: Arc<RWLock<Entities>>, inputQueue: ~Receiver<ActionMessage>) -> DriverSystem {
        DriverSystem{ents: ents, inputQueue: inputQueue, ais: Vec::new()}
    }

    fn convert_action_message(message: ActionMessage) -> Option<DriverActionEvent> {
        fn action_to_driver_action(act: KeyAction) -> Option<DriverAction>{
            match act {
                input::MoveLeft => Some(driver::Left),
                input::MoveUp => Some(driver::Forward),
                input::MoveDown => Some(driver::Back),
                input::MoveRight => Some(driver::Right),
                input::Fire => Some(driver::Fire),
                _ => None,
            }
        }
        match message {
            input::Press(action) => {
                action_to_driver_action(action).map(|da| driver::Start(da))
            },
            input::Release(action) => {
                action_to_driver_action(action).map(|da| driver::Stop(da))
            },
            _ => None,
        }
    }
}

impl System for DriverSystem{
    fn get_ids(&self) -> ~HashSet<uint> {
        let accel_ids: HashSet<uint> = self.ents.read().accel.ids.clone();
        let driver_ids: HashSet<uint> = self.ents.read().driver.ids.clone();
        ~accel_ids.intersection(&driver_ids).map(|i| *i).collect()
    }
    fn update(&mut self, d_time: f64){
        let ids = self.get_ids();
        let ents = &self.ents.read();
        let mut inputActions = ~Vec::new();
        //Handle keyboard input
        'recv: loop{
            match self.inputQueue.try_recv() {
                Ok(action) => {
                    match DriverSystem::convert_action_message(action) {
                        Some(a) => inputActions.push(a),
                        None => {},
                    }
                },
                _ => {break 'recv;}
            }
        }
        for cur_id in ids.iter(){
            let id = *cur_id;
            match self.ents.read().driver.find(id){
                None => {},
                Some(drv) => {
                    let mut driver = drv.write();
                    let events: &Vec<DriverActionEvent> = match driver.inputSource{
                        driver::AI(id) => {~Vec::new()},
                        driver::Human => {
                            inputActions.clone()
                        },
                    };
                    let ents = self.ents.read();
                    let accel_lock_opt = ents.accel.find(id);
                    if accel_lock_opt.is_none() {continue;}
                    let accel_lock = accel_lock_opt.unwrap();
                    //TODO
                    //Set acceleration
                    //Handle Firing
                    if events.len() != 0 {
                        for evt in events.iter() {
                            match *evt{
                                driver::Start(driver::Forward) => {
                                    let mut accel = accel_lock.write();
                                    accel.accel.y += driver.params.accel;
                                },
                                driver::Stop(driver::Forward) => {
                                    let mut accel = accel_lock.write();
                                    accel.accel.y -= driver.params.accel;
                                },
                                driver::Start(driver::Back) => {
                                    let mut accel = accel_lock.write();
                                    accel.accel.y -= driver.params.accel;
                                },
                                driver::Stop(driver::Back) => {
                                    let mut accel = accel_lock.write();
                                    accel.accel.y += driver.params.accel;
                                },
                                driver::Start(driver::Left) => {
                                    let mut accel = accel_lock.write();
                                    accel.accel.x -= driver.params.accel;
                                },
                                driver::Stop(driver::Left) => {
                                    let mut accel = accel_lock.write();
                                    accel.accel.x += driver.params.accel;
                                },
                                driver::Start(driver::Right) => {
                                    let mut accel = accel_lock.write();
                                    accel.accel.x += driver.params.accel;
                                },
                                driver::Stop(driver::Right) => {
                                    let mut accel = accel_lock.write();
                                    accel.accel.x -= driver.params.accel;
                                },
                                _ => {},
                            }
                            driver.update_action(evt);
                        }
                    }
                },
            }
        }
    }
}
