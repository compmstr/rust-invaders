extern crate sync;

use components::{ComponentContainer};
use components::physics::{Position, Velocity, Size, Acceleration};
use components::game::{Health};
use components::sprite::Sprite;
use components::driver::Driver;
use std::sync::atomics::{AtomicUint, SeqCst};

pub struct Entities{
    next_entity_id: AtomicUint,
    pub pos: ComponentContainer<Position>,
    pub vel: ComponentContainer<Velocity>,
    pub accel: ComponentContainer<Acceleration>,
    pub size: ComponentContainer<Size>,
    pub health: ComponentContainer<Health>,
    pub sprite: ComponentContainer<Sprite>,
    pub driver: ComponentContainer<Driver>,
}
impl Entities{
    pub fn new() -> Entities{
        Entities{
            next_entity_id: AtomicUint::new(0),
            pos: ComponentContainer::<Position>::new(),
            vel: ComponentContainer::<Velocity>::new(),
            accel: ComponentContainer::<Acceleration>::new(),
            size: ComponentContainer::<Size>::new(),
            health: ComponentContainer::<Health>::new(),
            sprite: ComponentContainer::<Sprite>::new(),
            driver: ComponentContainer::<Driver>::new(),
        }
    }
    pub fn create(&mut self, f: |uint, &mut Entities|) -> uint{
        let id = {self.next_id()};
        f(id, self);
        id
    }
    fn next_id(&self) -> uint {
        self.next_entity_id.fetch_add(1, SeqCst)
    }
    pub fn remove(&mut self, id: uint){
        self.pos.remove(id);
        self.vel.remove(id);
        self.accel.remove(id);
        self.size.remove(id);
        self.health.remove(id);
        self.sprite.remove(id);
        self.driver.remove(id);
    }
}
