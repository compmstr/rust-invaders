use collections::hashmap::HashSet;
pub mod physics;
pub mod collision;
pub mod graphics;
pub mod driver;

pub trait System{
    fn update(&mut self, d_time: f64){}
    fn get_ids(&self) -> ~HashSet<uint>;
}
